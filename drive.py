import serial
import struct
import argparse
import base64
from datetime import datetime
import os
import shutil
import numpy as np
import socketio
import eventlet
import eventlet.wsgi
from PIL import Image
from flask import Flask
from io import BytesIO
from keras.models import load_model
import utils
import socket

i=0
oldAngle = 0.0
newAngle = 0.0
oldAcc = 0.0
newAcc = 0.0
oldBrk = 0.0
newBrk = 0.0
sSteps = 0.0
aSteps = 0.0
bSteps = 0.0
ser = serial.Serial ('COM4', baudrate=9600)
sio = socketio.Server()

app = Flask(__name__)

model = None
prev_image_array = None

#set min/max speed for our autonomous car
MAX_SPEED = 25
MIN_SPEED = 10

#and a speed limit
speed_limit = MAX_SPEED
def sAnglesToSteps(angles):
    global oldAngle, newAngle
    sAngle = angles*(600)
    newAngle = sAngle - oldAngle
    steps = newAngle/1.8
    oldAngle = sAngle
    return steps

def throttleToAngle(throttle):
   global newAcc, oldAcc
   acc = (540)*throttle ##change acc to the calculations
   newAcc = int(acc - oldAcc)
   steps = newAcc/1.8
   oldAcc = acc
   return steps

def brakeToAngle(brake)
   global oldBrk, newBrk
   brk = (540)*brake ##change acc to the calculations(200*1.5)
   newBrk = int(brk - oldBrk)
   steps = newBrk/1.8
   oldBrk = brk
   return steps

#registering event handler for the server
@sio.on('telemetry')
def telemetry(sid, data):
    if data:

        steering_angle = float(data["steering_angle"])
        # The current throttle of the car, how hard to push peddle
        throttle = float(data["throttle"])
        # The current speed of the car
        speed = float(data["speed"])
        # The current image from the center camera of the car
        image = Image.open(BytesIO(base64.b64decode(data["image"])))
        try:
            image = np.asarray(image)       # from PIL image to numpy array
            image = utils.preprocess(image) # apply the preprocessing
            image = np.array([image])       # the model expects 4D array

            # predict the steering angle for the image
            steering_angle = float(model.predict(image, batch_size=1))

            global speed_limit
            if speed > speed_limit:
                speed_limit = MIN_SPEED  # slow down
            else:
                speed_limit = MAX_SPEED
            throttle = 1.0 - steering_angle**2 - (speed/speed_limit)**2

            sSteps = sAnglesToSteps(steering_angle)
            sSteps = (sSteps+334)
            steer = str(sSteps)
            global i
            if (throttle >= 0):
                aSteps = brakeToAngle(throttle)
                aSteps = int((aSteps +300))
                accel = str(aSteps)
                aSpeed = accel


            elif (throttle < 0):
                '''
                aSteps was unresolved over here, adding it via brakeToAngle function. 
                Change as necessary 
                '''
                aSteps = brakeToAngle(throttle)
                bSteps = throttleToAngle(abs(throttle))
                aSteps = int((aSteps + 300))
                brake = str(bSteps)
                aSpeed = brake

            '''
            i was unresolved over here, so added a i = 0 in the function parameter 
            '''



            if i == 5:
                '''
                aSpeed had a local scope, so added aSpeed = 0 in the function definition 
                '''
                i = 0
                serData = str(steer + aSpeed)
                # if(ser.readline()== b'ready\r\n'):
                ser.write(serData.encode())
                ##sleep(2)



            #ser.write(b'', steering_angle)
            #ser.write('hello')
            #print('Sent:'steering_angle)
           #print('{} {} {}'.format (steering_angle, throttle, speed))
            i += 1
            send_control(steering_angle, throttle)

        except Exception as e:
            print(e)

        # save frame
        if args.image_folder != '':
            timestamp = datetime.utcnow().strftime('%Y_%m_%d_%H_%M_%S_%f')[:-3]
            image_filename = os.path.join(args.image_folder, timestamp)
            image.save('{}.jpg'.format(image_filename))
    else:

        sio.emit('manual', data={}, skip_sid=True)


@sio.on('connect')
def connect(sid, environ):
    print("connect ", sid)
    send_control(0, 0)


def send_control(steering_angle, throttle):
    sio.emit(
        "steer",
        data={
            'steering_angle': steering_angle.__str__(),
            'throttle': throttle.__str__()
        },
        skip_sid=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Remote Driving')
    parser.add_argument(
        'model',
        type=str,
        help='Path to model h5 file. Model should be on the same path.'
    )
    parser.add_argument(
        'image_folder',
        type=str,
        nargs='?',
        default='',
        help='Path to image folder. This is where the images from the run will be saved.'
    )
    args = parser.parse_args()

    #load model
    model = load_model(args.model)

    if args.image_folder != '':
        print("Creating image folder at {}".format(args.image_folder))
        if not os.path.exists(args.image_folder):
            os.makedirs(args.image_folder)
        else:
            shutil.rmtree(args.image_folder)
            os.makedirs(args.image_folder)
        print("RECORDING THIS RUN ...")
    else:
        print("NOT RECORDING THIS RUN ...")

    # wrap Flask application with engineio's middleware
    app = socketio.Middleware(sio, app)

    # deploy as an eventlet WSGI server
    eventlet.wsgi.server(eventlet.listen(('', 4567)), app)
    # i+=1
